% classdefs.cls - style for the Tech Squares Class weekly handouts
% with the definitions of the calls taught that week
% $Id: classdefs.cls,v 1.5 2015/11/09 00:36:03 kchen Exp $
% By Stephen Gildea <gildea@alum>
% Converted from classdefs.sty December 1997
% 1998/01/29 previous version
% 1999/02/17: changed subtitle "Spring 1998" to "Spring 1999"
% 1999/06/02 add copyright to Acrobat doc info.
% 1999/10/01 add Table of Contents
% 2003/08/22: changed footer "Fall 1999" to "Fall 2003"


\NeedsTeXFormat{LaTeX2e}[1995/12/01]    %for \LoadClassWithOptions

\ProvidesClass{classdefs}[2005/12/04 Tech Squares Class weekly handouts]
\DeclareOption{nolicense}{\newcommand{\nolicense}{1}}
\ProcessOptions
\LoadClassWithOptions{article}          %load our base class
\RequirePackage{techlogo}
\RequirePackage{fancyhdr}
\RequirePackage{textcomp}               %do \textcopyright in Adobe Times

%%% Page layout

\headheight 0pt
\headsep 0pt
\topmargin 0pt
\textheight 9in

\raggedbottom                           %be more flexible about where p. 1 ends

\oddsidemargin 0in
\textwidth 6.5in

\setlength\parskip{.7\baselineskip}
\setlength\parindent{0in}

\newlength\titlevskip
\setlength\titlevskip{2\baselineskip}

% different weeks have different amounts of text and require
% different margins and spacing to look the best.

\newcommand{\pagesqueeze}{%
  \voffset -0.3in
  \textheight=9.4in
  \parskip=0in
}

\newcommand{\pagelongest}{%
  \voffset -0.3in
  \textheight=9.4in
  \parskip=0.4\parskip
  \titlevskip=1.3\titlevskip}

\newcommand{\pagelonger}{%
  \parskip=0.7\parskip
  \titlevskip=0.3\titlevskip}

\newcommand{\pagelong}{%
  \parskip=0.7\parskip}

\newcommand{\pagenormal}{\relax}


\newcommand{\pagenarrow}{%
  \textwidth=6.0in
  \oddsidemargin=0.25in}

\newcommand{\pagenarrower}{%
  \textwidth=5.5in
  \oddsidemargin=0.5in
  \parskip=1.2\parskip
  \titlevskip=2\titlevskip}

\newcommand{\pagenarrowest}{%
  \textwidth=5in
  \oddsidemargin=0.75in
  \parskip=2\parskip
  \titlevskip=3\titlevskip}

% use 1 for the shortest documents, and 7 for the longest.
% use 8 for really squeezing.
\newcommand{\documentlength}[1]{%
  \ifcase #1 % 0 case isn't used
  \or \pagenarrowest
  \or \pagenarrower
  \or \pagenarrow
  \or \pagenormal
  \or \pagelong
  \or \pagelonger
  \or \pagelongest
  \or \pagesqueeze
  \fi}




\footskip .2in
\AtBeginDocument{\font\sigfont=\fontname\font\space at 3pt}
\def\datetag: <#1Id: #2 #3 #4/#5/#6 #7>{%
  \gdef\tmstamp{#4/#5}\gdef\clsdf@year{#4}\gdef\@author{#7}}
\def\@author{}
\def\clsdf@year{}

%% Headers and Footers

\pagestyle{fancy}
\fancyhf{}                      %initialize to empty
\renewcommand{\headrulewidth}{0pt} %default is a headrule
% first (and normally only) page
\fancypagestyle{classdefsfrontpage}{
  \lfoot{\scriptsize \tmstamp}
  \cfoot{\scriptsize \textcopyright\ \clsdf@year\
    Massachusetts Institute of Technology}
  \ifdefined \nolicense \else
  \rfoot{\scriptsize \href{http://creativecommons.org/licenses/by-sa/4.0/}{\includegraphics[scale=0.5]{images/cc-by-sa}}}
  \fi
}
% subsequent pages (if any)
\lfoot{\scriptsize Week \@weeknum\ Page \thepage}
%\rfoot{\sigfont \tmstamp}           %on every page
\AtBeginDocument{\setlength{\headwidth}{\textwidth}}

% The 1999/02/06 requirement is for the 6.48 feature allowing
% me to put \hypersetup commands in \maketitle, after I've
% defined what \@subtitle is.
% 1999/09/14 6.66e fixes a problem where \addcontentsline added spaces.
% 2003/01/22 6.73n allows commas in \hyperref args (contents.tex uses this)
\RequirePackage{hyperref}[2003/01/22 v6.73n]

\hypersetup{pdfpagemode=UseNone} %no outline

% Whitespace tweaking in lists: http://tex.stackexchange.com/questions/86054/how-to-remove-the-whitespace-before-itemize-enumerate
\RequirePackage{enumitem}
\setlist[enumerate]{nosep}

%%% Special commands

% Use Adobe fonts for better, smaller PDF files
\RequirePackage{times}
%\newcommand\degr{$^\circ$}
% Use Adobe fonts for better PDF files
%\newcommand\degr{{\fontencoding{U}\fontfamily{psy}\selectfont\char'260}}
% With the times package, this simpler definition from the textcomp
%  package uses the Times Roman font instead of the Adobe Symbol font.
\newcommand\degr{\textdegree}

\special{header=hp-printer-duplex.ps}


\newcommand\weeknum[1]{\gdef\@weeknum{#1}%
  \clsdf@sethandout{#1}%
  \clsdf@addcontentsline{toc}{section}{Week \@weeknum}{Week \@weeknum}%
}

% These two are equal unless more than one week is printed
% on the same handout, as happens on the last handout.
% Then \weeknum appears in the middle of the text.
\def\@weeknum{0}                %the week the call is taught
\def\@handoutnum{0}             %the handout it appears on

\newcommand\clsdf@sethandout[1]{\def\@handoutnum{#1}}
% We want only the first \weeknum in a document to affect the handout number.
\AtBeginDocument{\renewcommand\clsdf@sethandout[1]{}}

% The star form \subtitle*{Name of Subtitle} suppresses the ToC entry
\newcommand{\subtitle}{\@ifstar{\clsdf@subtitlestar}{\clsdf@subtitle}}
\newcommand{\clsdf@subtitle}[1]{\clsdf@addcontentsline{toc}{section}{#1}{#1}%
  \clsdf@subtitlestar{#1}}
\newcommand{\clsdf@subtitlestar}[1]{\gdef\@subtitle{#1}}
\def\@subtitle{}

% the basic command: \call{Call Name}
% Typesets the call name in bold, uppercase: CALL NAME:
% Generates an index entry for the call in the case given.
% optional arg specifies alt index entry: \call[Index Entry]{Call Name}
% Additional index entries may be given with \index commands after \call

% \callmod{Call Name}{restriction} expands to
% CALL NAME (restriction):
% and builds an appropriate default index entry.

\def\call{\@dblarg\clsdf@callx}

\def\clsdf@callx[#1]#2{\clsdf@callinner{#1}{#2}{#2}:}

\def\callmod{\@ifnextchar[\clsdf@callmodxi\clsdf@callmodx}

\def\clsdf@callmodx#1#2{\clsdf@callmodxi[#1!#2]{#1}{#2}}

\def\clsdf@callmodxi[#1]#2#3{\clsdf@callinner{#1}{#2 (#3)}{#2} (#3):}

% #1 is index entry,
% #2 is text for table of contents (title plus any restriction from \callmod),
% #3 is title to typeset here
\def\clsdf@callinner#1#2#3{\clsdf@setthecall{#1}\index{#1}
  \par \vskip.5\baselineskip plus 1pt \penalty-400
  %% Position the PDF mark not so far in the left margin (hyperref 6.71v)
  %% (bug first reported 21 Oct 1999 against hyperref 6.66m).
  \leavevmode
  \hbox to 0pt{\hskip 2cm
    \clsdf@addcontentsline{toc}{subsection}{#2}{\clsdf@thecall}\hss}%
  {\bf \uppercase{#3}}}

% We remember the call we are in for writing the index entry.
% We need the info to make href's back to here for the HTML.
% But remove any parts after a @ for neatness
%   and after ! for consistency of references
\def\clsdf@setthecall#1{\makeatother\clsdfsetthecallx#1@\endargs\makeatletter}
\def\clsdfsetthecallx#1@#2\endargs{\clsdf@setthecally#1!\endargs}
\def\clsdf@setthecally#1!#2\endargs{\def\clsdf@thecall{#1}%
  \make@stripped@name{#1}\edef\clsdf@strippedcall{\newname}}

% \subheading is used to display "Week n" on a multi-week handout.
% Typically such handouts also use \subtitle to override the default subtitle.
% The \leavevmode is so that a \weeknum command inside the subheading
% argument will create a correctly-positioned PDF destination.
\newcommand\subheading[1]{\leavevmode{\Large\bf #1\par}}

%%% Header

\settechlogosize{1in}

\title{\techlogo \hskip 0.4em \raisebox{.25in}{Class}}

\subtitle*{Definitions for Week \@weeknum}
\def\clsdf@subtitleadd{}

\renewcommand\maketitle{
  \begin{center}
  \parskip=0pt
  \leavevmode
  {\Huge \@title}

  \vskip \titlevskip
  {\Large\bf \@subtitle\clsdf@subtitleadd}

  \end{center}

  \hypersetup{pdftitle=Tech Squares Class: \@subtitle}
  \hypersetup{pdfauthor={\@author.  \ \ Copyright 1995,1998,\clsdf@year\ MIT.}}
  \hypersetup{pdfsubject=square dance definitions}

  \thispagestyle{classdefsfrontpage}       %normally the only page
}

%%% Lesson Index

\makeindex

\def\clsdf@thecall{}            %initialize
\def\clsdf@strippedcall{}

% If there is a !, then the part before it needs
% to be duplicated in ;'s also, so that makeindex will
% combine this entry with the plain major entry.
\def\@wrindex#1{\clsdf@wrindex#1!!\endargs}
\def\clsdf@wrindex#1!#2!#3\endargs{%
  \edef\clsdf@temp{#2}\ifx\clsdf@temp\@empty
    \clsdf@wrindex@inner{#1}%
  \else
    \clsdf@wrindex@inner{#1;#1;!#2}%
  \fi}

% This is most like the original \@wrindex, but
% adds ";\clsdf@thecall;" to what is written out
% (this is used only by the HTML generator),
% and writes \@weeknum instead of \thepage.
\def\clsdf@wrindex@inner#1{%
   \protected@write\@indexfile{}%
      {\string\indexentry{#1;\clsdf@thecall;}{\@weeknum}}%
 \endgroup
 \@esphack}

% drop the part of the name between the semicolons
\def\clsdf@idxitem#1;#2;{\@idxitem #1}
\def\clsdf@subitem#1;#2;{\nopagebreak\clsdf@@subitem #1}
\def\clsdf@subsubitem#1;#2;{\nopagebreak\clsdf@@subsubitem #1}
\let\clsdf@@subitem\subitem
\let\clsdf@@subsubitem\subsubitem

%(start of work to make anchors from index work.
% Would need to massage link names as the perl file does.)
%{\catcode`\^^M=12%
% \gdef\clsdf@idxitem{\bgroup\catcode`\^^M=12 \clsdf@idxitemm}%
% \gdef\clsdf@idxitemm#1;#2;, #3#4^^M{%
%   \@idxitem #1, \clsdf@href{#3#4}{#1}\egroup}}
%\def\clsdf@href#1#2{\special{html:<a href="lesson#1.pdf###2">}%
%  #1\special{html:</a>}}

\def\theindex{
    \columnseprule \z@
    \columnsep 35pt
    \parindent\z@
    \parskip\z@ plus .3pt
    \let\item\clsdf@idxitem
    \let\subitem\clsdf@subitem
    \let\subsubitem\clsdf@subsubitem}
\def\endindex{}


%%% Lesson Table of Contents

\def\clsdf@addcontentsline#1#2#3#4{%
  \clsdf@@addcontentsline{#1}{#2}{#3}{#4}%
  % for some experiments, use the line below instead of the normal line above
  %\addcontentsline{#1}{#2}{#3}%
}

% This version of \addcontentsline writes the week number instead of
% the page number and adds a 4th argument giving the name to be
% used by the HTML generator.
\def\clsdf@@addcontentsline#1#2#3#4{%
  \make@stripped@name{#4}%      %put sanitized version into \newname
  %% We don't use hyperref's version because we want the .toc file
  %%  to contain xref info useful for the separately-generated HTML
  %%  file, not the PDF of this file.
  \addtocontents{#1}{\protect\contentsline{#2}{#3}{\@handoutnum}{\newname}}%
  \pdfbookmark[\csname toclevel@#2\endcsname]{#3}{\newname}%
}

% The ToC doesn't yet point to the correct external document,
% so suppress the links by overriding hyperref's \contentsline override
\def\contentsline#1#2#3#4{%
  %\csname l@#1\endcsname{#2}{#3}%
  \csname l@#1\endcsname{#2}{#3}{#4}%
  %\hyperref{file:lesson#3.pdf}{#1}{#4}{\csname l@#1\endcsname{#2}{#3}}%
  %\href{http:lesson#3.pdf}{\csname l@#1\endcsname{#2}{#3}}%
}

\hypersetup{bookmarksopen}

% the ToC is all links, and the borders are too distracting
\hypersetup{pdfborder=0 0 0}

% Open the .toc file, but inside a group with \contentsline redefined
% to not do anything so we don't create any ToC.
% We need to open the .toc file so that it gets updated with our entries.
% We will read the .toc file separately to make a ToC of all lessons.
\AtBeginDocument{{\def\contentsline#1#2#3#4{}\@starttoc{toc}}}

% It sucks that we have include this, and change it for where
% we think the destination might be. 
%\def\clsdf@baseurl{http://www.mit.edu/activities/tech-squares/lessons/}
\def\clsdf@baseurl{file:}

% We label each lesson a section and each call a subsection.

% Hyperlinks in PDF files to other PDF documents don't work very well
% when they are all served over the web.

\renewcommand*\l@section[3]{\pagebreak[1]\noindent
  %\textbf{\href{\clsdf@baseurl lesson#2.pdf}{#1}}%
  \textbf{#1}%
  \par}

\renewcommand*\l@subsection[3]{{\parindent=0.5em
  %\hyperref{\clsdf@baseurl lesson#2.pdf}{}{#3.2}{#1}%
  #1%
  \par}}

% gildea's modified version of \make@stripped@name from hyperref 6.73i 
% (or from hyperref 2003-12-03 6.74m -- they're the same)
% to strip more characters: slash, left paren, and right paren.
% This must be consistent with the classdefs.perl's sub callinternal.
\def\make@stripped@name#1{%
  \begingroup
    \escapechar\m@ne
    \global\let\newname\@empty
    \protected@edef\Hy@tempa{#1}%
    \edef\@tempb{%
      \noexpand\@tfor\noexpand\Hy@tempa:=%
        \expandafter\strip@prefix\meaning\Hy@tempa
    }%
    \@tempb\do{%
      \if{\Hy@tempa\def\Hy@tempa{}\fi
      \if}\Hy@tempa\def\Hy@tempa{}\fi
      \if/\Hy@tempa\def\Hy@tempa{}\fi
      \if(\Hy@tempa\def\Hy@tempa{}\fi
      \if)\Hy@tempa\def\Hy@tempa{}\fi
      \xdef\newname{\newname\Hy@tempa}%
    }%
  \endgroup
}
